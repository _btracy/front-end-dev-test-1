'use strict';

let gulp = require('gulp');
let watch = require('gulp-watch');
let sass = require('gulp-sass');
let concat = require('gulp-concat');
let uglify = require('gulp-uglify');
let imagemin = require('gulp-imagemin');
let autoprefixer = require('gulp-autoprefixer');

    gulp.task('scss', function () {
        return gulp.src('./dev/styles/styles.scss')
            .pipe(autoprefixer({
                cascade: false
            }))
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('./prod/styles'));
    });

    gulp.task('css', function () {
        return gulp.src('./dev/styles/jquery.fancybox.css')
            .pipe(autoprefixer({
                cascade: false
            }))
            .pipe(gulp.dest('./prod/styles'));
    });

    gulp.task('font', function () {
        return gulp.src('./dev/fonts/*.ttf')
            .pipe(gulp.dest('./prod/fonts'));
    });

    // Minify and copy all JavaScript (except vendor scripts)
    gulp.task('scripts', function() {
        return gulp.src('./dev/scripts/*.js')
            .pipe(uglify())
            .pipe(concat('bundle.min.js'))
            .pipe(gulp.dest('prod/scripts'));
    });

    // Copy all static images
    gulp.task('images', function() {
        return gulp.src('./dev/images/*')
            .pipe(imagemin({optimizationLevel: 5}))
            .pipe(gulp.dest('prod/images'));
    });

    gulp.task('watch', function() {
        watch('./dev/styles/styles.scss', function() {
            gulp.run(['scss', 'scripts']);
        });
    });


    gulp.task('default', ['scss', 'font', 'scripts', 'images', 'css']);
