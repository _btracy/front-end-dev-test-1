$( document ).ready(function() {

    $("#image-gallery").on('click', function() {
        $.fancybox.open([
            {
                src  : './prod/images/stoneland-gallery-1.jpg',
                opts : {
                    caption : ''
                }
            },
            {
                src  : './prod/images/stoneland-gallery-2.jpg',
                opts : {
                    caption : ''
                }
            },
            {
                src  : './prod/images/stoneland-gallery-3.jpg',
                opts : {
                    caption : ''
                }
            },
            {
                src  : './prod/images/stoneland-gallery-4.jpg',
                opts : {
                    caption : ''
                }
            },
            {
                src  : './prod/images/stoneland-gallery-5.jpg',
                opts : {
                    caption : ''
                }
            }
        ], {
            loop : false,
            hash : "image-gallery"
        });
    });
});


